//
// Created by user on 23.12.2017.
//

#include "exceptions/ShaderProgramCreationException.h"

ShaderProgramCreationException::ShaderProgramCreationException() : BaseException(
        "ShaderProgramCreationException:\nfailed to create shader program") {

}
