//
// Created by user on 23.12.2017.
//

#ifndef OPENGL_SHADERPROGRAMCREATIONEXCEPTION_H
#define OPENGL_SHADERPROGRAMCREATIONEXCEPTION_H


#include "BaseException.h"

class ShaderProgramCreationException : public BaseException {
public:
    ShaderProgramCreationException();
};


#endif //OPENGL_SHADERPROGRAMCREATIONEXCEPTION_H
